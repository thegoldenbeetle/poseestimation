import torch.onnx
import torch
from config import train_image_size
from model import PoseSimpleModel
import argparse

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--model-path', default="./lightning_logs/version_25/checkpoints/epoch=38-step=79365.ckpt")
    args = parser.parse_args()

    model = PoseSimpleModel.load_from_checkpoint(args.model_path).cpu()
    model.eval()
    
    x = torch.randn(1, 3, *train_image_size, requires_grad=True)
    output = model(x)

    torch.onnx.export(
        model,               
        x,                         
        "pose_estimation_last.onnx",   
        export_params=True,       
        opset_version=10,        
        do_constant_folding=True, 
        input_names = ['input'],  
        output_names = ['output'], 
        dynamic_axes={'input' : {0 : 'batch_size'},    
                      'output' : {0 : 'batch_size'}}
    )