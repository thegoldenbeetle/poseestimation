import json
import matplotlib.pyplot as plt
import numpy as np
import torchvision.transforms.functional as F


def load_json(p):
    with open(p, 'r') as fi:
        d = json.load(fi)
    return d


def show(imgs):
    if not isinstance(imgs, list):
        imgs = [imgs]
    fig, axs = plt.subplots(ncols=len(imgs), squeeze=False)
    for i, img in enumerate(imgs):
        img = img.detach()
        img = F.to_pil_image(img)
        axs[0, i].imshow(np.asarray(img))
        axs[0, i].set(xticklabels=[], yticklabels=[], xticks=[], yticks=[])