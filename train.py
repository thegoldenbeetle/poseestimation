from dataset import FreiHANDDataset, collate
from config import dataset_path, train_mode, val_mode
from torch.utils.data import DataLoader
from model import PoseSimpleModel
import lightning.pytorch as pl
from torchvision.io import read_image


pl.seed_everything(100)

if __name__ == "__main__":

    train_dataset = FreiHANDDataset(dataset_path, train_mode)
    val_dataset = FreiHANDDataset(dataset_path, val_mode)

    train_dataloader = DataLoader(
        train_dataset,
        collate_fn=collate,
        batch_size=16,
        shuffle=True,
        num_workers=8,
    )
    val_dataloader = DataLoader(
        val_dataset,
        collate_fn=collate,
        batch_size=16,
        shuffle=False,
        num_workers=8,
    )

    test_image = read_image(str(dataset_path / "evaluation" / "rgb" / "00000000.jpg"))
    pose_model = PoseSimpleModel(test_image)

    checkpoint_callback = pl.callbacks.ModelCheckpoint(
        save_top_k=3, monitor="val_loss", mode="min"
    )
    trainer = pl.Trainer(
        devices=1,
        accelerator="gpu",
        max_epochs=40,
        callbacks=[checkpoint_callback],
    )

    trainer.fit(
        model=pose_model,
        train_dataloaders=train_dataloader,
        val_dataloaders=val_dataloader,
    )
