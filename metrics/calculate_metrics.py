import pandas as pd
import numpy as np
import argparse


def calculate_mse_mae(gt_values, pred_values):
    gt_values = gt_values.iloc[:, 1:].values.astype(int)
    pred_values = pred_values.iloc[:, 1:].values.astype(int)
    mse = np.mean((gt_values - pred_values)**2)
    mae = np.mean(np.abs(gt_values - pred_values))
    return mse, mae

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-g', '--gt-file', default="./metrics/gt_2d_poses_real.txt")
    parser.add_argument('-p', '--pred-file', default="./metrics/mediapipe_preds_real.txt")
    args = parser.parse_args()

    gt_values = pd.read_csv(args.gt_file, header=None)
    pred_values = pd.read_csv(args.pred_file, header=None)

    na_mask = pred_values.iloc[:, 1].isna()
    pred_values = pred_values[~na_mask]
    gt_values = gt_values[~na_mask]

    print(len(gt_values), len(pred_values))

    mse, mae = calculate_mse_mae(gt_values, pred_values)
    print(mse, mae)
 