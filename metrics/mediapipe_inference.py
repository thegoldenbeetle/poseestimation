import cv2
import mediapipe as mp
from pathlib import Path
from tqdm import tqdm
import argparse


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--image-path', default="./FreiHAND_pub_v2/test")
    parser.add_argument('-o', '--output-path', default="./metrics/mediapipe_preds_frei_hand_eval.txt")
    args = parser.parse_args()

    hands_handler = mp.solutions.hands.Hands(max_num_hands=1, min_detection_confidence=0.0, static_image_mode=True)
    images = Path(args.image_path).glob('*.jpg')

    f = open(args.output_path, "w")

    for image_path in tqdm(sorted(images)):

        f.write(f"{image_path.name}")
        image = cv2.imread(str(image_path))

        image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        results = hands_handler.process(image_rgb)

        if results.multi_hand_landmarks:
            hand_landmarks = results.multi_hand_landmarks[0]
            if not hand_landmarks.landmark:
                print("No landmarks")
            for idx, landmark in enumerate(hand_landmarks.landmark):
                h, w, c = image.shape
                cx, cy = int(landmark.x * w), int(landmark.y * h)
                f.write(f",{cx},{cy}")
        else:
            print("No hands")

        f.write("\n")
    
    f.close()
