import cv2
import onnx
import onnxruntime
import numpy as np
import matplotlib.pyplot as plt
import time
from tqdm import tqdm
from pathlib import Path
import argparse


def preprocess_image(img_orig):
    img = cv2.cvtColor(img_orig, cv2.COLOR_BGR2RGB)
    h, w, c = img.shape
    img = cv2.resize(img, dsize=(224, 224), interpolation=cv2.INTER_AREA)
    ratio_w = w / 224
    ratio_h = h / 224
    
    img = img.astype('float32')

    mean = 255 * [0.485, 0.456, 0.406]
    std = 255 * [0.229, 0.224, 0.225]

    img[..., 0] -= mean[0]
    img[..., 1] -= mean[1]
    img[..., 2] -= mean[2]

    img[..., 0] /= std[0]
    img[..., 1] /= std[1]
    img[..., 2] /= std[2]

    img = np.transpose(img, (2, 0, 1))

    img = img.reshape((1, 3, 224, 224))
    return img, (ratio_h, ratio_w)


def get_coordinate(mask, ratios):
    idx = np.unravel_index(np.argmax(mask), mask.shape)
    return int(idx[0] * ratios[0]), int(idx[1] * ratios[1]) 


def get_coordinates(masks, ratios):
    coords = []
    for mask in masks:
        coords_i = get_coordinate(mask, ratios)
        coords.extend(coords_i)
    
    return coords


def inference(image, session):
    preprocessed_model_input, ratios = preprocess_image(image)
    result = session.run(["output"], {"input": preprocessed_model_input})
    result_masks = result[0][0]
    coordinates = get_coordinates(result_masks, ratios)
    return coordinates


def draw_coords(image, coords, output_path):
    for i in range(0, len(coords), 2):
        cv2.circle(image, (coords[i + 1], coords[i]), 3, (255, 0 , 0), -1)
    cv2.imwrite(output_path, image)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--model-path', default="./pose_estimation_last.onnx")
    parser.add_argument('-i', '--image-path', default="./FreiHAND_pub_v2/test")
    parser.add_argument('-o', '--output-path', default="./metrics/my_model_last_preds_frei_hand_eval.txt")
    parser.add_argument('-r', '--recursive', action="store_true")
    args = parser.parse_args()

    session = onnxruntime.InferenceSession(args.model_path, None)    

    if not args.recursive:
        image = cv2.imread(str(args.image_path))
        coords = inference(image, session)
        draw_coords(image, coords, args.output_path)
    else:            
        images = Path(args.image_path).glob('*.jpg')
        f = open(args.output_path, "w")

        for image_path in tqdm(sorted(images)):
            f.write(f"{image_path.name}")
            image = cv2.imread(str(image_path))
            coordinate = inference(image, session)
            if len(coordinate) // 2 != 21:
                print(len(coordinate) // 2)
            for i in range(len(coordinate) // 2):
                f.write(f",{coordinate[i + 1]},{coordinate[i]}")

            f.write("\n")
        
        f.close()