# Pose Estimation

## Обучение модели

В качестве модели взята Unet с Resnet бэкбоном, голова заменена на сверточный слой, дающий на выходе 21канальное изображение heatmapов ключевых точек.

В качестве датасета использовался [FreiHAND](https://lmb.informatik.uni-freiburg.de/projects/freihand/). Для каждого изображения ожидается 21 точка позы руки.

В качестве лос функции взята MSE.

Графики процесса обучения:

|  Train loss   |   Val loss   |    Test sample |
| ------------- | ------------ |----------------|
| ![](./imgs/train_loss.png) |![](./imgs/val_loss.png) | ![](./imgs/test_sample.png) |



## Сравнение с open source решениями

Было произведено сравнение с моделями [NSRMhand](https://github.com/HowieMa/NSRMhand) и [Mediapipe](https://developers.google.com/mediapipe/solutions/vision/hand_landmarker) на validation части датасета, используемого для обучения, а также на датасете [real dataset](https://github.com/3d-hand-shape/hand-graph-cnn/tree/master).

Результаты приведены в таблице.

| Модель        | MAE (Frei Hand) [224x224] | MAE (Real) [256x256] |
| ------------- | ------------------------- | -------------------- |
| Unet + Resnet | 21.5                      | 46.3                 |
| NSRMhand      | 13.1                      | 41.5                 |
| Mediapipe     | 5.9                       | 4.4                  |



## Приложение виртуальная доска

В качестве модели взята mediapipe как лучшая.

![demo](./imgs/app.gif)
