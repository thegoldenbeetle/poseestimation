from pathlib import Path

# dataset
dataset_path = Path("./FreiHAND_pub_v2/")
image_size = (224, 224)
train_image_size = (224, 224)
train_mode = "training"
val_mode = "evaluation"
train_db_size = 32560