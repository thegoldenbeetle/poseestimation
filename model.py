import lightning as L
import torch
import torch.nn as nn
from torch.nn import functional as F
import segmentation_models_pytorch as smp
from dataset import process


class PoseSimpleModel(L.LightningModule):
    def __init__(self, test_image=None):
        super().__init__()
        self.model = smp.Unet(
            encoder_name="resnet34",
            encoder_weights="imagenet",
            in_channels=3,
            classes=1,
            activation="sigmoid",
        )
        self.model.segmentation_head = nn.Sequential(
            torch.nn.Conv2d(
                16, 21, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)
            ),
            nn.Sigmoid()
        )
        self.test_image = test_image

    def forward(self, x):
        return self.model.forward(x)

    def training_step(self, batch, batch_nb):
        x, y = batch
        preds = self.model(x)
        loss = torch.nn.MSELoss()(preds, y)
        self.log("train_loss", loss)
        return loss

    def validation_step(self, batch, batch_nb):
        x, y = batch
        preds = self.model(x)
        loss = torch.nn.MSELoss()(preds, y)
        self.log("val_loss", loss)

        if self.test_image is not None:
            test_prediction = self.model(
                torch.unsqueeze(process(self.test_image), 0).cuda()
            )
            test_prediction = torch.squeeze(test_prediction)
            points_image = torch.unsqueeze(torch.clamp(torch.sum(test_prediction, dim=0), max=1.0), 0)
            points_image = torch.cat([points_image, points_image, points_image], dim=0)   
            self.logger.experiment.add_image(
                "Val image",
                points_image,
                self.global_step,
            )
        return loss

    def configure_optimizers(self):
        return torch.optim.Adam(self.parameters())