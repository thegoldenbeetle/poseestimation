from torch.utils.data import DataLoader, Dataset
import torch
import os
import pathlib
import numpy as np
import json
from utils import load_json, show
from config import image_size, train_mode, val_mode, train_db_size, train_image_size
from torchvision.io import read_image
import torchvision.transforms as T
import matplotlib.pyplot as plt
from torchvision.utils import make_grid
from torch.utils.data.dataloader import default_collate
import torchvision.transforms.functional as F
import random


MEAN = [0.485, 0.456, 0.406]
STD = [0.229, 0.224, 0.225]
ANGLES = [0, 90, 180, 270]


def projectPoints(xyz, K): 
    xyz = np.array(xyz) 
    K = np.array(K) 
    uv = np.matmul(K, xyz.T).T 
    return uv[:, :2] / uv[:, -1:]


def collate(data):
    data_list = [x.to_list_to_train() for x in data]
    return default_collate(data_list)


def process(image):
    norm_image = T.Normalize(mean=MEAN, std=STD)(image.float())
    return T.Resize(size=train_image_size)(norm_image)
            

class DataItem():

    def __init__(self, image_path, K, xyz, to_transform=False):
        
        self.image_path = image_path
        self.K = K
        self.xyz = xyz

        self.image = read_image(str(self.image_path))
        self.norm_image = T.Normalize(mean=MEAN, std=STD)(self.image.float())
        self.output = self.create_output()
        
        self.image = T.Resize(size=train_image_size)(self.image)
        self.norm_image = T.Resize(size=train_image_size)(self.norm_image)
        self.output = T.Resize(size=train_image_size)(self.output)

        if to_transform:
            if random.random() > 0.5:
                self.image = F.hflip(self.image)
                self.norm_image = F.hflip(self.norm_image)
                self.output = F.hflip(self.output)
            if random.random() > 0.5:
                self.image = F.vflip(self.image)
                self.norm_image = F.vflip(self.norm_image)
                self.output = F.vflip(self.output)
            angle = random.choice(ANGLES)
            self.image = F.rotate(self.image, angle)
            self.norm_image = F.rotate(self.norm_image, angle)
            self.output = F.rotate(self.output, angle)

    def create_output(self):
        projected_points = projectPoints(self.xyz, self.K)[:, ::-1]
        blurrer = T.GaussianBlur(kernel_size=(25, 25), sigma=2.5)
        result = torch.zeros(len(projected_points), *image_size)
        for i, (x, y) in enumerate(projected_points):
            if int(x) >= image_size[0] or int(x) < 0 or int(y) >= image_size[1] or int(y) < 0:
                continue
            result[i, int(x), int(y)] = 1.0
        result = blurrer(result)
        result /= torch.max(result)
        return result
    
    def show_item(self):
        image = self.image
        points_image = torch.unsqueeze(torch.clamp(torch.sum(self.output, dim=0), max=1.0), 0)
        points_image = torch.cat([points_image, points_image, points_image], dim=0)
        show(image)
        show(points_image)
        plt.show()

    def to_list_to_train(self):
        return [self.norm_image, self.output]


class FreiHANDDataset(Dataset):

    def __init__(self, root_dir, mode=train_mode):
        
        self.root_dir = root_dir
        self.mode = mode

        k_path = root_dir / f"{mode}_K.json"
        xyz_path = root_dir / f"{mode}_xyz.json"
        self.K_list = load_json(k_path)
        self.xyz_list = load_json(xyz_path) 
        self.images_path = root_dir / f"{mode}" / "rgb"

    def __len__(self):
        return len(self.xyz_list)

    def __getitem__(self, idx):

        if self.mode == train_mode:
            image_mode = random.randint(0, 3)
            image_idx = idx + image_mode * train_db_size
            annotations_idx = idx
            to_transfrom = True
        else:
            image_idx = idx
            annotations_idx = idx
            to_transfrom = False

        data_item = DataItem(
            self.images_path / f'{image_idx:08d}.jpg', 
            self.K_list[annotations_idx], 
            self.xyz_list[annotations_idx],
            to_transform=to_transfrom
        )
        return data_item


