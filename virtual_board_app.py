import cv2
import mediapipe as mp


FOREFINGER_IDX = 8

def draw_history(image, history):
    for i, (cx, cy) in enumerate(history):
        if i < len(history) - 1:
            cv2.line(image, (cx, cy), history[i + 1], (0, 255, 0), 2) 
        cv2.circle(image, (cx, cy), 5, (255, 0, 0), cv2.FILLED)


if __name__ == "__main__":

    cap = cv2.VideoCapture(0)
    hands_handler = mp.solutions.hands.Hands(max_num_hands=1)

    history = []

    while True:
        success, image = cap.read()
        image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        results = hands_handler.process(image_rgb)

        if results.multi_hand_landmarks:
            hand_landmarks = results.multi_hand_landmarks[0]
            for idx, landmark in enumerate(hand_landmarks.landmark):
                h, w, c = image.shape
                cx, cy = int(landmark.x * w), int(landmark.y * h)
                if idx == FOREFINGER_IDX:
                    history.append((cx, cy))

        draw_history(image, history)
        cv2.imshow("Frame", image)
        
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()